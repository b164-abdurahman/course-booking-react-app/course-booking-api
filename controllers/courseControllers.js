const Course = require("../models/Course");

//Create a new course
/*
Steps:
1. Create a new Course object
2. Save to the database
3. error handling
*/


module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object to our database
	return newCourse.save().then((course, error) => {
		//Course creation failed
		if(error) {
			return false;
		} else {
			//Course Creation successful
			return true;
		}
	})

}

//Answer
// module.exports.addCourse = (reqBody) => {

// 	console.log(reqBody);

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }




//Retrieving All courses

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


//Retrieve all ACTIVE courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}

//Retrieve SPECIFIC course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}


//Update a course
/*
Steps:
1. Create variable which will contain the information retrieved from the request body
2. find and update course using the course ID
*/

module.exports.updateCourse = (courseId, reqBody) => {
	//specify the properties of the doc to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		//course not updated
		if(error) {
			return false;
		}else {
			//course updated successfully
			return true;
		}
	})

}



//Archive a course

module.exports.archiveCourse = (reqParams) => {
	//object

	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams, updateActiveField).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}















